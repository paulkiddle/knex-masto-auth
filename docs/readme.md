# Knex Masto Auth

Mastodon authentication using a knex instance for data persistance.
Automatically registers a client with previously unknown instances and
persists them to your database for future use.

```javascript
import KnexMastoAuth, { migrate } from 'knex-masto-auth';
import Knex from 'knex';

// Bring your own knex instance
const db = new Knex({
	client: 'sqlite3',
  connection: {
    filename: './db.sqlite3',
  },
});

// Create the database tables if they're absent (idempotent operation)
await migrate(db);

// Get the instance of MastoAuth
const auth = new KnexMastoAuth(db, {
	clientName: 'My App',
	redirectUri: 'http://my-app.example/exchange-token'
});


// Example server - for full details of how to use the `auth` instance, see the [masto-auth library](https://gitlab.com/paulkiddle/masto-auth#module_masto-auth.default)
export default async (req, res) => {
	const { pathname, searchParams } = new URL('file://' + req.url);

	if(pathname === '/login') {
		// Get the instance to login to, e.g. through form submission
		const instance = searchParams.get('instance') || 'https://kith.kitchen';

		// Get the redirect URL - this will also handle issuer lookup and client registration if needed
		const instanceAuthPage = await auth.getRedirectUrl(instance);

		// Redirect to instance log in page
		res.setHeader('location', instanceAuthPage);
		res.statusCode = 303;
		res.end();
	} else if(pathname === '/exchange-token') {
		// User will be redirected here after authentication
		// Get the user info and do what you want with it
		res.end(JSON.stringify(await auth.getUserFromCallback(req)))
	} else {
		res.statusCode = 404;
		res.end('Not found');
	}
}

```
