import MastoAuth from 'masto-auth';
import KnexSettings from 'knex-settings';

const _table = table => (table ?? 'mastodon_clients');

/**
 * Knex Masto Auth
 * @module knex-masto-auth
 * @see {@link module:knex-masto-auth.default}
 */

/**
 * Instance of the knex library
 * @external Knex
 * @see {@link https://knexjs.org/}
 */

/**
 * Instance of the MastoAuth library
 * @external MastoAuth
 * @see {@link https://gitlab.com/paulkiddle/masto-auth#module_masto-auth.default}
 */

/**
 * @typedef ClientOptions
 * @param {string} redirectUri	The URI to redirect the user to after they have authenticated on their mastodon instance.
 * @param {string} clientName	The name of your application
 */

/**
 * @callback CatchError
 * Callback for catching a mastodon error.
 * Whatever is returned or thrown from the callback will be returned or thrown to the caller.
 * @param {Error} error The error that occured
 * @param {string} url The issuer URL that the error occured for
 */

/**
 * @alias module:knex-masto-auth.default
 */
export default class KnexMastoAuth extends MastoAuth {
	/**
	 * Create a new instance of KnexMastoAuth
	 * @param {external:Knex}	knex	Instance of the Knex library
	 * @param {Object} clientOptions Client options
	 * @param {Function} [options.catchError] Optional function to call when an error occurs in the client registration process
	 * @param {Function} [options.tableName] Name of the sql table to use to store mastodon client data. Defaults to 'mastodon_clients'
	 * @returns {external:MastoAuth}
	 */
	constructor(knex, clientOptions, { catchError, tableName }={}, mastoOptions){
		tableName = _table(tableName);

		const storage = KnexSettings(knex, tableName);
		super(async uri => {
			const client = await storage.get(uri);

			if(client) {
				return JSON.parse(client);
			}

			try {
				const client = await MastoAuth.register(uri, clientOptions, mastoOptions);
				await storage.set(uri, JSON.stringify(client));
				return client.toJSON();
			} catch(e) {
				if(!catchError) {
					throw e;
				}
				return catchError(e, uri);
			}
		}, mastoOptions);
	}

	/**
	 * Create the storage table if it doesn't exist
	 * and return a new instance of KnexMastoAuth
	 * @memberof module:knex-masto-auth.default
	 * @param {external:Knex}	knex	Instance of the Knex library
	 * @param {Function} [options.catchError] Optional function to call when an error occurs in the client registration process
	 * @param {Function} [options.tableName] Name of the sql table to use to store mastodon client data. Defaults to 'mastodon_clients'
	 * @returns {external:MastoAuth}
	 */
	static async migrate(knex, clientOptions, generalOptions, mastoOptions){
		await migrate(knex, generalOptions && generalOptions.tableName);

		return new KnexMastoAuth(knex, clientOptions, generalOptions, mastoOptions);
	}
}

/**
 * Create the storage table if it doesn't exist
 * @see module:knex-masto-auth.default#migrate
 */
export async function migrate(knex, table){
	table = _table(table);

	if(!await knex.schema.hasTable(table)) {
		await KnexSettings(knex, table).migrate_up();
	}
}
