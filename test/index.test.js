import KMA from '../src/index.js';
import knex from 'knex-sqlite';
import {jest} from '@jest/globals';

test('Registers client', async ()=>{
	const k = await KMA.migrate(knex(':memory:'), {
		redirectUri: 'http://localhost/redirect'
	}, undefined, {
		request(url){
			return {
				end(){
					return this;
				},
				on(name, cb){
					cb([JSON.stringify({
						'https://example.com/api/v1/instance': {
							uri: 'example.com'
						}
					}[url] || {
						client_id: 'abc',
						redirect_uri: 'http://localhost/redirect'
					})]);
				}
			};
		}
	});

	const url = await k.getRedirectUrl('https://example.com/');

	expect(url).toEqual('https://example.com/oauth/authorize?scope=read&client_id=abc&redirect_uri=http%3A%2F%2Flocalhost%2Fredirect&response_type=code');

	const url2 = await k.getRedirectUrl('https://example.com/');

	expect(url2).toEqual('https://example.com/oauth/authorize?scope=read&client_id=abc&redirect_uri=http%3A%2F%2Flocalhost%2Fredirect&response_type=code');
});

test('Handle errors', async ()=>{
	const db = knex(':memory:');
	const options = { tableName: 'my_table' };

	const k = await KMA.migrate(db, {
		redirectUri: 'http://localhost/redirect'
	}, options, {
		request(){
			return {
				end(){
					return this;
				},
				on(name, cb){
					if(name === 'error') {
						cb(new Error('An error'));
					}
				}
			};
		}
	});

	await expect(k.getRedirectUrl('https://example.com/')).rejects.toBeInstanceOf(Error);

	options.catchError = jest.fn((e)=>{
		throw e;
	});

	const k2 = await KMA.migrate(db, {
		redirectUri: 'http://localhost/redirect'
	}, options, {
		request(){
			return {
				end(){
					return this;
				},
				on(name, cb){
					if(name === 'error') {
						cb(new Error('An error'));
					}
				}
			};
		}
	});

	await expect(k2.getRedirectUrl('https://example.com/')).rejects.toBeInstanceOf(Error);

	expect(options.catchError).toHaveBeenCalledWith(expect.any(Error), 'https://example.com');
});
