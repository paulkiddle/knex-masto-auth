# Generate API docs
./node_modules/.bin/jsdoc2md src/*.js > docs/api.md

echo "\n## Dependencies" > docs/dependencies.md
node -e "console.log(Object.entries(require('./package.json').dependencies||{}).map(e=>' - '+e.join(': ')).join('\n') || 'None')" >> docs/dependencies.md

cat docs/readme.md docs/dependencies.md docs/api.md > README.md
