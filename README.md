# Knex Masto Auth

Mastodon authentication using a knex instance for data persistance.
Automatically registers a client with previously unknown instances and
persists them to your database for future use.

```javascript
import KnexMastoAuth, { migrate } from 'knex-masto-auth';
import Knex from 'knex';

// Bring your own knex instance
const db = new Knex({
	client: 'sqlite3',
  connection: {
    filename: './db.sqlite3',
  },
});

// Create the database tables if they're absent (idempotent operation)
await migrate(db);

// Get the instance of MastoAuth
const auth = new KnexMastoAuth(db, {
	clientName: 'My App',
	redirectUri: 'http://my-app.example/exchange-token'
});


// Example server - for full details of how to use the `auth` instance, see the [masto-auth library](https://gitlab.com/paulkiddle/masto-auth#module_masto-auth.default)
export default async (req, res) => {
	const { pathname, searchParams } = new URL('file://' + req.url);

	if(pathname === '/login') {
		// Get the instance to login to, e.g. through form submission
		const instance = searchParams.get('instance') || 'https://kith.kitchen';

		// Get the redirect URL - this will also handle issuer lookup and client registration if needed
		const instanceAuthPage = await auth.getRedirectUrl(instance);

		// Redirect to instance log in page
		res.setHeader('location', instanceAuthPage);
		res.statusCode = 303;
		res.end();
	} else if(pathname === '/exchange-token') {
		// User will be redirected here after authentication
		// Get the user info and do what you want with it
		res.end(JSON.stringify(await auth.getUserFromCallback(req)))
	} else {
		res.statusCode = 404;
		res.end('Not found');
	}
}

```

## Dependencies
 - knex-settings: ^2.0.1
 - masto-auth: ^1.1.0
<a name="module_knex-masto-auth"></a>

## knex-masto-auth
Knex Masto Auth

**See**: [default](#module_knex-masto-auth.default)  

* [knex-masto-auth](#module_knex-masto-auth)
    * _static_
        * [.default](#module_knex-masto-auth.default)
            * [new module.exports(knex, clientOptions)](#new_module_knex-masto-auth.default_new)
            * [.migrate(knex)](#module_knex-masto-auth.default.migrate) ⇒ [<code>MastoAuth</code>](#external_MastoAuth)
        * [.migrate()](#module_knex-masto-auth.migrate)
    * _inner_
        * [~ClientOptions](#module_knex-masto-auth..ClientOptions)
        * [~CatchError](#module_knex-masto-auth..CatchError) : <code>function</code>
        * [~Knex](#external_Knex)
        * [~MastoAuth](#external_MastoAuth)

<a name="module_knex-masto-auth.default"></a>

### knex-masto-auth.default
**Kind**: static class of [<code>knex-masto-auth</code>](#module_knex-masto-auth)  

* [.default](#module_knex-masto-auth.default)
    * [new module.exports(knex, clientOptions)](#new_module_knex-masto-auth.default_new)
    * [.migrate(knex)](#module_knex-masto-auth.default.migrate) ⇒ [<code>MastoAuth</code>](#external_MastoAuth)

<a name="new_module_knex-masto-auth.default_new"></a>

#### new module.exports(knex, clientOptions)
Create a new instance of KnexMastoAuth


| Param | Type | Description |
| --- | --- | --- |
| knex | [<code>Knex</code>](#external_Knex) | Instance of the Knex library |
| clientOptions | <code>Object</code> | Client options |
| [options.catchError] | <code>function</code> | Optional function to call when an error occurs in the client registration process |
| [options.tableName] | <code>function</code> | Name of the sql table to use to store mastodon client data. Defaults to 'mastodon_clients' |

<a name="module_knex-masto-auth.default.migrate"></a>

#### default.migrate(knex) ⇒ [<code>MastoAuth</code>](#external_MastoAuth)
Create the storage table if it doesn't exist
and return a new instance of KnexMastoAuth

**Kind**: static method of [<code>default</code>](#module_knex-masto-auth.default)  

| Param | Type | Description |
| --- | --- | --- |
| knex | [<code>Knex</code>](#external_Knex) | Instance of the Knex library |
| [options.catchError] | <code>function</code> | Optional function to call when an error occurs in the client registration process |
| [options.tableName] | <code>function</code> | Name of the sql table to use to store mastodon client data. Defaults to 'mastodon_clients' |

<a name="module_knex-masto-auth.migrate"></a>

### knex-masto-auth.migrate()
Create the storage table if it doesn't exist

**Kind**: static method of [<code>knex-masto-auth</code>](#module_knex-masto-auth)  
**See**: module:knex-masto-auth.default#migrate  
<a name="module_knex-masto-auth..ClientOptions"></a>

### knex-masto-auth~ClientOptions
**Kind**: inner typedef of [<code>knex-masto-auth</code>](#module_knex-masto-auth)  

| Param | Type | Description |
| --- | --- | --- |
| redirectUri | <code>string</code> | The URI to redirect the user to after they have authenticated on their mastodon instance. |
| clientName | <code>string</code> | The name of your application |

<a name="module_knex-masto-auth..CatchError"></a>

### knex-masto-auth~CatchError : <code>function</code>
**Kind**: inner typedef of [<code>knex-masto-auth</code>](#module_knex-masto-auth)  

| Param | Type | Description |
| --- | --- | --- |
| error | <code>Error</code> | The error that occured |
| url | <code>string</code> | The issuer URL that the error occured for |

<a name="external_Knex"></a>

### knex-masto-auth~Knex
Instance of the knex library

**Kind**: inner external of [<code>knex-masto-auth</code>](#module_knex-masto-auth)  
**See**: [https://knexjs.org/](https://knexjs.org/)  
<a name="external_MastoAuth"></a>

### knex-masto-auth~MastoAuth
Instance of the MastoAuth library

**Kind**: inner external of [<code>knex-masto-auth</code>](#module_knex-masto-auth)  
**See**: [https://gitlab.com/paulkiddle/masto-auth#module_masto-auth.default](https://gitlab.com/paulkiddle/masto-auth#module_masto-auth.default)  
